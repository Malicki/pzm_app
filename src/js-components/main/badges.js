import {h} from './h'

export function getNewBadge(task) {
  if(!task.created) return '';

  var date = new Date(task.created);
  var time = (new Date()).getTime();
  var diff = time - date.getTime();

  if(diff < (60*60*1000))
    return h('span',{className:'newTaskBadge'});
  else '';
}