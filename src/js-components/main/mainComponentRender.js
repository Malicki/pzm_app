import {h} from './h'
import {getNewBadge} from './badges'

export function render(state, ctx){
  return h('main',{ className:'todosMain'},[
    h('label',{},[
      h('input',{
        type:'checkbox',
        checked: state.todoOnly,
        onchange: ctx.onTodoOnlyChange
      }), 'Pokaż tylko do zrobienia'
    ]),
    h('label',{},[
      'sortowanie',
      h('select',{
        onchange: ctx.onSort
      },[
        h('option',{value:'Id', selected:state.order == 'Id'},['domyślne']),
        h('option',{value:'name', selected:state.order == 'name'},['Nazwa']),
        h('option',{value:'done', selected:state.order == 'done'},['Zrobione']),
        h('option',{value:'public', selected:state.order == 'public'},['Publiczne'])
      ]),
      h('select',{
        onchange: ctx.onSortDir
      },[
        h('option',{value:'asc', selected:state.orderAsc},['rosnąco']),
        h('option',{value:'desc', selected:!state.orderAsc},['malejąco'])
      ])]),
    h('ul',{className:'todosTasks'},
      state.tasks.map(function(task){
        return h('li',{className:'todosTask'},[
          h('label',{ attr: {
            'data-id': task.id,
            'data-etag': task.etag,
            'data-done': task.done,
            'data-public': task.public || false
          }},[
            h('input',{
              type:'checkbox',
              onchange: ctx.onTaskChange,
              checked: task.done
            },[]),
            getNewBadge(task),
            task.name + (task.public ? ' - publiczne' : '')
          ])
        ])
      })
    ),
    h('div',{},[
      h('button',{
        type:'button',
        onclick: ctx.prevPage
      }, ['<']),
      h('span',{},[''+(state.page+1)]),
      h('button',{
        type:'button',
        onclick: ctx.nextPage
      }, ['>'])
    ]),
    h('div',{className: 'todosActions'},[
      h('label',{},['zadanie',
      h('input',{
        className:'todosNewTask',
        value: state.text,
        onchange: ctx.onTextChange
      }),
      h('label',{},[
        'Powiadom',
        h('input',{
          type: 'email',
          className:'todosNewTask',
          value: state.text,
          onchange: ctx.onNotifyChange
        })
      ])
    ]),
    h('label',{},[
      h('input',{
        type:'checkbox',
        checked: state.public,
        onchange: ctx.onPublicChange
      }), 'Publiczne'
    ]),
    h('button', {
      className: 'todosAddTask',
      onclick: ctx.addTask,
      type:'button'
    },
    ['Dodaj'])
    ])
  ]);
}

export function pointLess(){
  //
}