export function saveTaskToServerAsync(task) {
  return new Promise(function(resolve,reject) {
    setTimeout(function(){
      if(!window.localStorage['todos']) {
        window.localStorage['todos'] = "[]";
      }
      var tasks = JSON.parse(window.localStorage['todos']);
      var newId = tasks.length;
      
      if(tasks.filter(function(t){
        return t.name == task.name;
      }).length > 0 ) {
        reject("serwer: masz już zadanie o takiej nazwie");
      }
      else {
        tasks.push({
          name: task.name,
          done: task.done,
          public: task.public,
          created: (new Date()).toISOString(),
          notify: task.notify || '',
          id: newId
        });

        window.localStorage['todos'] = JSON.stringify(tasks);

        resolve({
          name: task.name,
          id: newId
        });
      }

      
    },500);
    

  })
  
}

export const maxTop = 10;

export function getTasksFromServerAsync(config){
  return new Promise(function(resolve,reject){
    setTimeout(function(){
      if(!window.localStorage['todos']) {
        window.localStorage['todos'] = "[]";
      }
      var tasks = JSON.parse(window.localStorage['todos']);
      var skip = 0;
      var top = config.top || maxTop;
      if(!!config) {
        var filter = config.filter;
        if(!!filter) {
          tasks = tasks.filter(function(task){
            return task.done == filter.done;
          })
        }
        tasks = tasks.sort(function(a,b){
          if(config.sortAsc)
            if(a[config.sort] > b[config.sort])
              return 1;
            else if(a[config.sort] < b[config.sort])
              return -1;
            else return 0;
          else
            if(a[config.sort] > b[config.sort])
              return -1;
            else if(a[config.sort] < b[config.sort])
              return 1;
            else return 0;
        })
        skip = (!!config.skiptoken) ? Number(config.skiptoken) : 0;
         {
          var returnSkiptoken = ((skip+top) < tasks.length );
          tasks = tasks.slice(skip);
        }

        
      }
      
      tasks = tasks.slice(0, top);
      var response = {
        value: tasks
      };
      if(returnSkiptoken) {
        response.skiptoken = skip+top;
      }
      resolve(response);
    },200);
  });
}

export function patchTaskAsync(id,partialTask) {
  return new Promise(function(resolve,reject){
    setTimeout(function(){
      if(!window.localStorage['todos']) {
        window.localStorage['todos'] = "[]";
      }
      var tasks = JSON.parse(window.localStorage['todos']);
      var task = tasks.filter(function(task){
        return task.id == id;
      });
      if(task.length >= 1)
        task = task[0];
      else task = null;

      if(!!task) {
        var properties = Object.keys(partialTask);
        properties.forEach(function(prop){
          task[prop] = partialTask[prop];
        })
        task.modified = (new Date()).toISOString();

        if(task.notify && task.done) {
          alert('użytkownik '+task.notify+' został powiadomiony o zrealizowaniu zadania');
        }
      }

      window.localStorage['todos'] = JSON.stringify(tasks);

      resolve();
    },200);
  });
}