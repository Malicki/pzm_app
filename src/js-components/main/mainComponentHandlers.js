import {patchTaskAsync} from './server'

export function onSort(ev){
  var self = this;
  var select = ev.target;
  self.state.order = select.selectedOptions[0].value;
  self.state.skiptokens = [];
  self.state.page = 0;
  self.load();
}

export function onSortDir(ev){
  var self = this;
  var select = ev.target;
  self.state.skiptokens = [];
  self.state.page = 0;
  self.state.orderAsc = select.selectedOptions[0].value == 'asc';
  self.load();
}

export function onNotifyChange(ev){
  var self = this;
  self.state.notify = ev.target.value;
}



export function onPublicChange(ev){
  var self = this;
  self.state.public = ev.target.checked;

}

export function onTodoOnlyChange(ev){
  var self = this;
  self.state.todoOnly = ev.target.checked;
  self.load();
}



export function nextPage(){
  var self = this;
  if(self.state.skiptokens.length == (self.state.page+1)) {
    self.state.page =self.state.page +1;
    self.load();
  }  
  
}
export function prevPage(){
  var self = this;
  if(self.state.page > 0) {
    self.state.page =self.state.page -1;
    self.state.skiptokens.pop();
    self.state.skiptokens.pop();
    self.load();
  }
}


export function onTextChange (ev){
  var self = this;
  self.state.text = ev.target.value;
}