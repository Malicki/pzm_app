const maxTop = 3;

export function shp(httpClient, version, absoluteUrl) {
  var self = this;
  self.client = httpClient;
  self.version = version;
  self.prefix = absoluteUrl;

  self.saveTaskToServerAsync = function(task) {
    return self.client.post(self.prefix
      +"/_api/web/lists/getbytitle('TodoList')/items",
      self.version,{
      body: JSON.stringify(task)
    }).then(function(response){
      return response.json();
    }).then(function(json){
      return json;
    })
  }

  self.getTasksFromServerAsync = function(config) {

    var query = [("$top="+maxTop)];

    if(!!config) {
      if(!!config.filter) {
        query.push("$filter=done eq "+(config.filter.done ? 'true' : 'false') );
      }
      if(!!config.sort) {
        query.push("$orderby="+config.sort + " "+(config.sortAsc ? 'asc' : 'desc') );
      }
    }

    
    

    var url = self.prefix+"/_api/web/lists/getbytitle('TodoList')/items";

    if(query.length > 0) {
      url = url + "?"+query.join("&");
    }

    if(!!config){
      if(!!config.skiptoken) {
        url = config.skiptoken;
      }
    }


    return self.client.get(url, self.version).then(function(response){
      return response.json();
    }).then(function(json){
      json.skiptoken = json["@odata.nextLink"];
      json.tasks = json.value.map(function(task){
        task.id = task.Id || task.ID;
        task.etag = task["@odata.etag"];
        return task;
      })
      return json;
    })
  }

  self.patchTaskAsync = function(id, etag, partialTask) {

    return self.client.fetch(self.prefix
      +"/_api/web/lists/getbytitle('TodoList')/items("+id+")",
      self.version,{
        method:'PATCH',
        headers: {
          'if-match':etag
        },
      body: JSON.stringify(partialTask)
    });
  }

  self.sendEmail = function(to,subject,body) {
    var obj = {
      "properties":{
        "To":  [to],
        "Subject":subject,
        "Body":body
      }
    }

    return self.client.post(self.prefix
      +"/_api/SP.Utilities.Utility.SendEmail",
      self.version,{
        body: JSON.stringify(obj)
      });
  }
}