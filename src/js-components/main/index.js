import {render} from './mainComponentRender'
import {h} from './h'
import {getNewBadge} from './badges'
import {data} from './data'
import {
  getTasksFromServerAsync,
  saveTaskToServerAsync,
  patchTaskAsync
} from './server'

import {
  onSort,
  onSortDir,
  onNotifyChange,
  onPublicChange,
  onTodoOnlyChange,
  nextPage,
  prevPage,
  onTextChange} from './mainComponentHandlers'

  export function MainComponent(parent, services) {
    var self = this;
    window["mycomponent"] = self;
    self.parent = parent;
    self.services = services;
    self.state = data();

    self.onSort = onSort.bind(self);
    self.onSortDir = onSortDir.bind(self);
    self.onNotifyChange = onNotifyChange.bind(self);
    self.onPublicChange = onPublicChange.bind(self);
    self.onTodoOnlyChange = onTodoOnlyChange.bind(self);
    self.nextPage = nextPage.bind(self);
    self.prevPage = prevPage.bind(self);
    self.onTodoOnlyChange = onTodoOnlyChange.bind(self);
    self.onTextChange = onTextChange.bind(self);
    
    self.render = function() {
      var r = render(self.state, self);
      if(!self.el) {
        self.parent.appendChild(r);
      }
      else {
        self.parent.replaceChild(r, self.el);
      }
      self.el = r;
    }

    self.load = function() {
      var todoOnly = self.state.todoOnly;
      var config = {
        sort : self.state.order,
        sortAsc: self.state.orderAsc
      };

      if(todoOnly) {
        config.filter = {
          done: false,
        }
      }

      if(self.state.page > 0) {
        config.skiptoken = self.state.skiptokens[self.state.skiptokens.length-1];
      }

      self.services.getTasksFromServerAsync(config).then(function(response){
        self.state.tasks = response.value;
        if(response.skiptoken) {
          self.state.skiptokens.push(response.skiptoken);
        }
        self.render();
      })
    }

    self.validateNewTask = function(task) {
      return task.name && task.name.length > 0 && task.name.length < 50;
    }

    
  self.onTaskChange = function(ev){
    var checked = ev.target.checked;
    var taskid = Number(ev.target.parentElement.getAttribute('data-id'));
    var etag = ev.target.parentElement.getAttribute('data-etag');

    self.services.patchTaskAsync(taskid,etag,{
      done: checked
    }).then(function(){
      if(checked) {
        var task = self.state.tasks.filter(function(t){
          return t.id == taskid;
        });
        if(task.length > 0) {
          task = task[0];
          if(!!task.notify) {
            self.services.sendEmail(task.notify,'realizacja zadania','zadanie '+task.name+'zostało zrealizowane');
          }
        }
      }
      self.load();
    })
  }

    self.addTask = function() {
      var task = {
        name: self.state.text,
        done: false,
        public: self.state.public,
        notify: self.state.notify
      };

      if(self.validateNewTask(task)) {
        self.services.saveTaskToServerAsync(task).then(function(task){
          self.state.skiptokens.pop();
          self.load();
          alert('Zadanie '+task.name+' zostało dodane ('+task.id+')');
        }).catch(function(comment){
          alert(comment);
        })
        
        self.state.text = '';
        self.state.notify = '';
        self.state.public = false;
      }
      else {
        alert('Zadanie musi mieć minimum 1 znak i maksymalnie 49 znaków.');
      }
    }
    self.load();
  }

