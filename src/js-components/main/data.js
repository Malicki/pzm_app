export function data() {
  return {
    text: '',
    todoOnly: false,
    skiptokens: [],
    order: 'Id',
    orders: ['name','done'],
    orderAsc: true,
    public: false,
    page: 0,
    tasks: [],
    notify: ''
  }
}