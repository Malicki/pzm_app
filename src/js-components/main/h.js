export function h(tag, props, children) {
  var el = document.createElement(tag);
  if(!!props) {
    var keys = Object.keys(props);
    keys.forEach(function(key){
      if(key === 'attr') {
        var attrObj = props[key];
        var attrs = Object.keys(attrObj);
        attrs.forEach(function(attr){
          el.setAttribute(attr, attrObj[attr])
        })
      }
      else {
        el[key] = props[key];
      }

    })
  }

  if(!!children) {
    for(var i=0; i<children.length; i++) {
      var child = children[i];
      if(!!child) {
        if(typeof child == 'string') {
          child = document.createTextNode(child);
        }
        el.appendChild(child);
      }
    }
  }
  return el;
}