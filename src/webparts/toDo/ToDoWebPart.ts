require('./workingStyles.scss');
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';


import * as strings from 'ToDoWebPartStrings';

import { SPHttpClient } from '@microsoft/sp-http';

import {MainComponent} from '../../js-components/main/index';
import {shp} from '../../js-components/main/shp';


export interface IToDoWebPartProps {
  description: string;
}

export default class ToDoWebPart extends BaseClientSideWebPart<IToDoWebPartProps> {

  //this.context.spHttpClient.post(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists`, SPHttpClient.configurations.v1, spOpts)
  public render(): void {
    var services = new shp(this.context.spHttpClient,
      SPHttpClient.configurations.v1,
      this.context.pageContext.web.absoluteUrl);
    (new MainComponent(this.domElement, services)).render();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
